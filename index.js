/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function getData(){
		let name = prompt("Enter your name: ");
		let age = prompt("Enter your age: ");
		let location = prompt("Where do you live: ");

		console.log("Hello", name);
		console.log("You are", age, "years old");
		console.log("You live in", location);
	}
	getData();

/*

	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function getArtist(){
		let artist1 = prompt("Enter your top 1 favorite band/artist");
		let artist2 = prompt("Enter your top 2 favorite band/artist");
		let artist3 = prompt("Enter your top 3 favorite band/artist");
		let artist4 = prompt("Enter your top 4 favorite band/artist");
		let artist5 = prompt("Enter your top 5 favorite band/artist");

		console.log("Your top 5 favorite band/artist:");
		console.log("1.", artist1);
		console.log("2.", artist2);
		console.log("3.", artist3);
		console.log("4.", artist4);
		console.log("5.", artist5);
	}
	getArtist();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function getMovies(){
		let movie1 = prompt("Enter your top 1 favorite movie");
		let rating1 = prompt("Enter Rotten Tomatoes Rating for your top 1 movie");
		let movie2 = prompt("Enter your top 2 favorite movie");
		let rating2 = prompt("Enter Rotten Tomatoes Rating for your top 2 movie");
		let movie3 = prompt("Enter your top 3 favorite movie");
		let rating3 = prompt("Enter Rotten Tomatoes Rating for your top 3 movie");
		let movie4 = prompt("Enter your top 4 favorite movie");;
		let rating4 = prompt("Enter Rotten Tomatoes Rating for your top 4 movie")
		let movie5 = prompt("Enter your top 5 favorite movie");
		let rating5 = prompt("Enter Rotten Tomatoes Rating for your top 5 movie");

		console.log("Your Top 5 Movies and Rotten Tomatoes Rating:")
		console.log("1.", movie1);
		console.log("Rotten Tomatoes Rating:", rating1);
		console.log("2.", movie2);
		console.log("Rotten Tomatoes Rating:", rating2);
		console.log("3.", movie3);
		console.log("Rotten Tomatoes Rating:", rating3);
		console.log("4.", movie4);
		console.log("Rotten Tomatoes Rating:", rating4);
		console.log("5.", movie5);
		console.log("Rotten Tomatoes Rating:", rating5);
	}
	getMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

printUsers();
	function printUsers(){
		alert("Hi! Please add the names of your friends.");
		let friend1 = prompt("Enter your first friend's name:"); 
		let friend2 = prompt("Enter your second friend's name:"); 
		let friend3 = prompt("Enter your third friend's name:");

		console.log("You are friends with:")
		console.log(friend1); 
		console.log(friend2); 
		console.log(friend3); 
};


// console.log(friend1);
// console.log(friend2);